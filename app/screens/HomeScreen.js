import React, { Component } from "react";
import {
  Alert,
  ImageBackground,
  Image,
  StyleSheet,
  TouchableOpacity,
  TouchableHighlight,
  TextInput,
  Text,
  View,
  StatusBar,
  Platform,
  KeyboardAvoidingView
} from "react-native";
import { Icon, Container, Header, Left, Right, Body, Title } from "native-base";
// import Loader from "./../Loader";
import Loader from "./Helper/Loader";
import LoginView from "./LoginScreen";
import Api from "../Api";
import Storage from "../Storage";
import { AppLoading, SplashScreen } from "expo";
import { Asset } from "expo-asset";
import Menu, { MenuItem } from "react-native-material-menu";
import { LinearGradient } from "expo-linear-gradient";
import Autocomplete from "react-native-autocomplete-input";

export default class Homescreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isSplashReady: false,
      isAppReady: false
    };
    global.userName = "";
    SplashScreen.preventAutoHide();
  }

  async componentDidMount() {
    this.setState({ loading: true });
    await this.getURLServer().then(respon => {
      global.urlServer = respon;
      this.setState({
        loading: false
      });
    });
  }

  fetchLogin = (username, password) => {
    global.userName = username;
    this.setState({
      username: username,
      loading: true
    });
    Api.fetchLogin(
      username,
      password,
      (onSuccess = response => {
        if (response.data != null) this.saveUserData(response.data);
        //Ambil daftar kode uakpb
        fetch(global.urlServer + "/api-android/get-listkodeuakpb")
          .then(res => res.json())
          .then(json => {
            const { results: listKodeUakpb } = json;
            this.setState({ listKodeUakpb });
          })
          .catch(e => {
            console.log("ERROR = " + e);
          });

        //Ambil daftar kode barang
        fetch(global.urlServer + "/api-android/get-listkodebarang")
          .then(res => res.json())
          .then(json => {
            const { results: listKodeBarang } = json;
            this.setState({ listKodeBarang });
          })
          .catch(e => {
            console.log("ERROR = " + e);
          });
        this.setState({ loading: false });
      }),
      (onError = error => {
        this.setState({ loading: false });
      })
    );
  };

  render() {
    if (this.state.loading) {
      return <Loader />;
    }

    if (!this.state.isSplashReady) {
      return (
        <AppLoading
          startAsync={this._cacheSplashResourcesAsync}
          onFinish={() => this.setState({ isSplashReady: true })}
          onError={console.warn}
          autoHideSplash={false}
        />
      );
    }

    if (!this.state.isAppReady) {
      if (this.state.userData == null) {
        return <LoginView fetchLogin={this.fetchLogin} />;
      }

      return (
        <Container
          style={{
            flex: 1
          }}
        >
          <ImageBackground
            style={{ flex: 1 }}
            source={require("./../../assets/backgrounds/home.jpg")}
          >
            <View
              style={{
                flex: 1,
                justifyContent: "space-around",
                alignItems: "center"
              }}
            >
              <View style={styles.grupTombol}>
                <View style={styles.containerTombol}>
                  <TouchableOpacity onPress={() => this.fetchListMonitoring()}>
                    <View style={styles.bingkaiTombol}>
                      <View style={styles.bingkaiGambar}>
                        <Image
                          source={require("./../../assets/images/correctlist.png")}
                          style={styles.gambarTombol}
                        />
                      </View>

                      <Text style={styles.teksTombol}>Daftar Monitor Aset</Text>
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={styles.containerTombol}>
                  <TouchableOpacity onPress={() => this.fetchQRScanner()}>
                    <View style={styles.bingkaiTombol}>
                      <View style={styles.bingkaiGambar}>
                        <Image
                          source={require("./../../assets/images/qrscan.png")}
                          style={
                            (styles.gambarTombol, { width: 65, height: 65 })
                          }
                        />
                      </View>

                      <Text style={styles.teksTombol}>Pindai Kode QR</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>

              <View style={styles.grupTombol}>
                <View style={styles.containerTombol}>
                  <TouchableOpacity onPress={() => this.fetchSearch()}>
                    <View style={styles.bingkaiTombol}>
                      <View style={styles.bingkaiGambar}>
                        <Image
                          source={require("./../../assets/images/search.png")}
                          style={styles.gambarTombol}
                        />
                      </View>

                      <Text style={styles.teksTombol}>Pencarian Manual</Text>
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={styles.containerTombol}>
                  <TouchableOpacity onPress={() => this.fetchInputSearch()}>
                    <View style={styles.bingkaiTombol}>
                      <View style={styles.bingkaiGambar}>
                        <Image
                          source={require("./../../assets/images/class.png")}
                          style={styles.gambarTombol}
                        />
                      </View>

                      <Text style={styles.teksTombol}>
                        Pencarian Berdasarkan Golongan
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>

              <View style={styles.grupTombol}>
                <TouchableOpacity onPress={() => this.logoutUser()}>
                  <View style={styles.bingkaiTombol}>
                    <View style={styles.bingkaiGambar}>
                      <Image
                        source={require("./../../assets/images/logout.png")}
                        style={styles.gambarTombol}
                      />
                    </View>

                    <Text style={styles.teksTombol}>Log Out</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </ImageBackground>
        </Container>
      );
    }

    return <Loader />;
  }
  //Menu-------------------------
  _menu = null;

  setMenuRef = ref => {
    this._menu = ref;
  };

  hideMenu = () => {
    this._menu.hide();
  };

  showMenu = () => {
    this._menu.show();
  };
  //--------------------
  setUserData = user => {
    global.userData = user;
    this.setState({
      userData: user
    });
    this.saveUserData(user);
    let userToken = this.getUserToken();
    this.setState({
      userToken: userToken
    });
    this.state.loading = false;
    this.setState({ init: false });
  };

  performTimeConsumingTask = async () => {
    return new Promise(resolve =>
      setTimeout(() => {
        resolve("result");
      }, 1000)
    );
  };

  async saveUserData(userData) {
    global.userData = userData;
    this.setState({
      userData: userData
    });
    return Storage.saveUserData(userData);
  }

  async getUserData() {
    return Storage.getUserData();
  }

  async getURLServer() {
    return Storage.getURLServer();
  }

  async saveURLServer(data) {
    dataUrlServer = { URLServer: data };
    return Storage.saveURLServer(dataUrlServer);
  }

  fetchQRScanner = () => {
    this.props.navigation.navigate("QRScreen", {
      name: "from parent"
    });
  };

  fetchSearch = () => {
    this.props.navigation.navigate("Search", {
      name: "from parent"
    });
  };

  fetchInputSearch = () => {
    this.props.navigation.navigate("InputSearch", {
      name: "from parent"
    });
  };

  fetchListMonitoring = () => {
    this.props.navigation.navigate("ListMonitoring", {
      name: "from parent"
    });
  };

  logoutUser = ok => {
    Storage.saveUserData(null);
    Storage.saveData("@token", null);
    Storage.saveData("@pin", null);
    this.setState({
      userData: null,
      userToken: null
    });
  };

  async getURLServer() {
    return await Storage.getData("@urlserver");
  }

  _cacheSplashResourcesAsync = async () => {
    const gif = require("./../../assets/bppt-logo.png");
    return Asset.fromModule(gif).downloadAsync();
  };

  _cacheResourcesAsync = async () => {
    SplashScreen.hide();
    const images = [
      require("./../../assets/bppt-logo.png"),
      require("./../../assets/backgrounds/home.jpg"),
      require("./../../assets/backgrounds/login.jpg")
    ];

    const cacheImages = images.map(image => {
      return Asset.fromModule(image).downloadAsync();
    });

    await Promise.all(cacheImages);
    this.setState({ isAppReady: true });
  };
}

const styles = StyleSheet.create({
  bingkaiTombol: {
    justifyContent: "center",
    alignItems: "center"
  },
  bingkaiGambar: {
    width: 100,
    height: 100,
    borderRadius: 75,
    borderWidth: 5,
    borderColor: "white",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "blue",
    shadowColor: "#808080",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5
  },
  gambarTombol: {
    flex: 1,
    resizeMode: "contain"
  },
  teksTombol: {
    color: "white",
    textAlign: "center"
  },
  grupTombol: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  containerTombol: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});
