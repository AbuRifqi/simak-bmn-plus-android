import React from "react";
import {
  Alert,
  Dimensions,
  ImageBackground,
  View,
  Text,
  StyleSheet,
  TouchableOpacity
} from "react-native";
import {
  Icon,
  Container,
  Header,
  Left,
  Right,
  Body,
  Title,
  Button
} from "native-base";
import SectionedMultiSelect from "react-native-sectioned-multi-select";
import { LinearGradient } from "expo-linear-gradient";
// import Loader from "./../Loader";
import Loader from "./Helper/Loader";
import Headers from "./Headers";

class InputSearchScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      daftarGolongan: [],
      daftarBidang: [],
      daftarKelompok: [],
      daftarSubKelompok: [],
      daftarSubSubKelompok: [],
      daftarUakpb: [],
      selectedGolongan: [],
      selectedBidang: [],
      selectedKelompok: [],
      selectedSubKelompok: [],
      selectedSubSubKelompok: [],
      selectedUakpb: [],
      kodeUakpb: "",
      kodeBarang: "",
      namaBarang: "",
      loading: false,
      loadingUakpb: false,
      loadingGolongan: false,
      loadingBidang: false,
      loadingKelompok: false,
      loadingSubKelompok: false,
      loadingSubSubKelompok: false
    };
  }

  async fetchListDropdown(apiUrl, onSuccess, onError) {
    // this.setState({ loading: true });
    let options = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    };

    fetch(apiUrl, options)
      .then(response => response.json())
      .then(response => {
        let listData = response.map(items => {
          return { id: items.code, name: items.text };
        });
        this.setState({
          loading: false
        });

        onSuccess(listData);
      })
      .catch(function(error) {
        Alert.alert(
          "Terjadi Kesalahan",
          "Alamat server keliru atau tidak ada koneksi. \n" + error.message,
          [
            {
              text: "OK",
              onPress: () => {
                this.setState({
                  loading: false,
                  loadingUakpb: false,
                  loadingGolongan: false,
                  loadingBidang: false,
                  loadingKelompok: false,
                  loadingSubKelompok: false,
                  loadingSubSubKelompok: false
                });
                onError(error.message);
              }
            }
          ],
          { cancelable: false }
        );
      });
  }

  async componentDidMount() {
    apiUrl = globalThis.urlServer + "/api-android/get-listkodeuakpb";
    this.setState({ loadingUakpb: true });
    let options = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    };

    fetch(apiUrl, options)
      .then(response => response.json())
      .then(response => {
        let listData = response.results.map(items => {
          return { id: items.kd_lokasi, name: items.kd_lokasi };
        });
        this.setState({
          daftarUakpb: listData,
          loadingUakpb: false
        });
      })
      .catch(function(error) {
        Alert.alert(
          "Terjadi Kesalahan",
          "Alamat server keliru atau tidak ada koneksi. \n" + error.message,
          [
            {
              text: "OK",
              onPress: () => {
                this.setState({
                  loading: false,
                  loadingUakpb: false
                });
              }
            }
          ],
          { cancelable: false }
        );
      });
    this.setState({ loadingGolongan: true });
    let apiUrl = globalThis.urlServer + "/api-android/list-golongan";
    this.fetchListDropdown(
      apiUrl,
      (onSuccess = lstGol => {
        this.setState({
          daftarGolongan: lstGol,
          loadingGolongan: false
        });
      }),
      (onError = error => {
        this.setState({ loadingGolongan: false });
        console.log("ERROR = " + error);
      })
    );
  }

  onSelectedUakpbChange = selectedItems => {
    this.setState(() => ({
      selectedUakpb: selectedItems,
      kodeUakpb: this.state.daftarUakpb.find(obj => obj.id == selectedItems)
        .name
    }));
  };

  onSelectedGolonganChange = selectedItems => {
    const id = selectedItems.toString();
    let apiUrl = globalThis.urlServer + "/api-android/list-bidang/?code=" + id;
    this.setState({ loadingBidang: true });
    this.fetchListDropdown(
      apiUrl,
      (onSuccess = lstData => {
        this.setState({
          daftarBidang: lstData,
          loadingBidang: false
        });
      }),
      (onError = error => {
        this.setState({
          loadingBidang: false
        });
        console.log("ERROR = " + error);
      })
    );

    this.setState(() => ({
      selectedGolongan: selectedItems
    }));
  };

  onSelectedBidangChange = selectedItems => {
    const id = selectedItems.toString();
    let apiUrl =
      globalThis.urlServer + "/api-android/list-kelompok/?code=" + id;

    this.setState({
      loadingKelompok: true
    });
    this.fetchListDropdown(
      apiUrl,
      (onSuccess = lstData => {
        this.setState({
          daftarKelompok: lstData,
          loadingKelompok: false
        });
      }),
      (onError = error => {
        this.setState({
          loadingKelompok: false
        });
        console.log("ERROR = " + error);
      })
    );

    this.setState(() => ({
      selectedBidang: selectedItems
    }));
  };

  onSelectedKelompokChange = selectedItems => {
    const id = selectedItems.toString();

    let apiUrl =
      globalThis.urlServer + "/api-android/list-sub-kelompok/?code=" + id;

    this.setState({
      loadingSubKelompok: true
    });
    this.fetchListDropdown(
      apiUrl,
      (onSuccess = lstData => {
        this.setState({
          daftarSubKelompok: lstData,
          loadingSubKelompok: false
        });
      }),
      (onError = error => {
        this.setState({
          loadingSubKelompok: false
        });
        console.log("ERROR = " + error);
      })
    );

    this.setState(() => ({
      selectedKelompok: selectedItems
    }));
  };

  onSelectedSubKelompokChange = selectedItems => {
    const id = selectedItems.toString();
    let apiUrl =
      globalThis.urlServer + "/api-android/list-subsub-kelompok/?code=" + id;

    this.setState({
      loadingSubSubKelompok: true
    });
    this.fetchListDropdown(
      apiUrl,
      (onSuccess = lstData => {
        this.setState({
          daftarSubSubKelompok: lstData,
          loadingSubSubKelompok: false
        });
      }),
      (onError = error => {
        this.setState({
          loadingSubSubKelompok: false
        });
        console.log("ERROR = " + error);
      })
    );

    this.setState(() => ({
      selectedSubKelompok: selectedItems
    }));
  };

  onSelectedSubSubKelompokChange = selectedItems => {
    this.setState(() => ({
      selectedSubSubKelompok: selectedItems,
      kodeBarang: selectedItems,
      namaBarang: this.state.daftarSubSubKelompok.find(
        obj => obj.id == selectedItems
      ).name
    }));
  };

  fetchListAsset = () => {
    this.props.navigation.navigate("ListAsset", {
      name: "from parent",
      kodeUakpb: this.state.kodeUakpb,
      kodeBarang: this.state.kodeBarang,
      namaBarang: this.state.namaBarang
    });
  };

  render() {
    if (this.state.loading) {
      return <Loader />;
    }

    return (
      <ImageBackground
        style={{ flex: 1, resizeMode: "stretch" }}
        source={require("./../../assets/backgrounds/home.jpg")}
      >
      {/* <Container> */}
        <Headers
          title={"Kriteria Pencarian"}
          navigation={this.props.navigation}
          handlePostClick={this.handlePostClick}
        />
        <View style={styles.container}>
          <View
            style={{
            }}
          >
            <View style={styles.dropdown}>
              <SectionedMultiSelect
                items={this.state.daftarUakpb}
                single={true}
                uniqueKey="id"
                selectText="Pilih Kode Wilayah..."
                searchPlaceholderText="Temukan kategori..."
                selectedText="terpilih"
                confirmText="Konfirmasi"
                removeAllText="Hapus Semua"
                loading={this.state.loadingUakpb}
                styles={{
                  chipText: {
                    maxWidth: Dimensions.get("screen").width - 90
                  },
                  // itemText: {
                  //   color: this.state.selectedItems.length ? 'black' : 'lightgrey'
                  // },
                  selectedItemText: {
                    color: "blue"
                  },
                  // itemText:{
                  //   color:"blue",
                  //   backgroundColor:"green"
                  // },
                  // subItemText: {
                  //   color: this.state.selectedItems.length ? 'black' : 'lightgrey'
                  // },
                  selectedSubItemText: {
                    color: "blue"
                  }
                }}
                showDropDowns={true}
                readOnlyHeadings={false}
                onSelectedItemsChange={this.onSelectedUakpbChange}
                selectedItems={this.state.selectedUakpb}
                showChips={false}
              />
            </View>
            <View style={styles.dropdown}>
              <SectionedMultiSelect
                items={this.state.daftarGolongan}
                single={true}
                uniqueKey="id"
                selectText="Pilih Golongan..."
                searchPlaceholderText="Temukan kategori..."
                selectedText="terpilih"
                confirmText="Konfirmasi"
                removeAllText="Hapus Semua"
                showDropDowns={true}
                readOnlyHeadings={false}
                onSelectedItemsChange={this.onSelectedGolonganChange}
                selectedItems={this.state.selectedGolongan}
                showChips={false}
                loading={this.state.loadingGolongan}
              />
            </View>
            <View style={styles.dropdown}>
              <SectionedMultiSelect
                items={this.state.daftarBidang}
                single={true}
                uniqueKey="id"
                selectText="Pilih Bidang..."
                searchPlaceholderText="Temukan kategori..."
                selectedText="terpilih"
                confirmText="Konfirmasi"
                removeAllText="Hapus Semua"
                showDropDowns={true}
                readOnlyHeadings={false}
                onSelectedItemsChange={this.onSelectedBidangChange}
                selectedItems={this.state.selectedBidang}
                showChips={false}
                loading={this.state.loadingBidang}
              />
            </View>

            <View style={styles.dropdown}>
              <SectionedMultiSelect
                items={this.state.daftarKelompok}
                single={true}
                uniqueKey="id"
                selectText="Pilih Kelompok..."
                searchPlaceholderText="Temukan kategori..."
                selectedText="terpilih"
                confirmText="Konfirmasi"
                removeAllText="Hapus Semua"
                showDropDowns={true}
                readOnlyHeadings={false}
                onSelectedItemsChange={this.onSelectedKelompokChange}
                selectedItems={this.state.selectedKelompok}
                showChips={false}
                loading={this.state.loadingKelompok}
              />
            </View>

            <View style={styles.dropdown}>
              <SectionedMultiSelect
                items={this.state.daftarSubKelompok}
                single={true}
                uniqueKey="id"
                selectText="Pilih Sub Kelompok..."
                searchPlaceholderText="Temukan kategori..."
                selectedText="terpilih"
                confirmText="Konfirmasi"
                removeAllText="Hapus Semua"
                showDropDowns={true}
                readOnlyHeadings={false}
                onSelectedItemsChange={this.onSelectedSubKelompokChange}
                selectedItems={this.state.selectedSubKelompok}
                showChips={false}
                loading={this.state.loadingSubKelompok}
              />
            </View>

            <View style={styles.dropdown}>
              <SectionedMultiSelect
                items={this.state.daftarSubSubKelompok}
                single={true}
                uniqueKey="id"
                selectText="Pilih Sub Sub Kelompok..."
                searchPlaceholderText="Temukan kategori..."
                selectedText="terpilih"
                confirmText="Konfirmasi"
                removeAllText="Hapus Semua"
                showDropDowns={true}
                readOnlyHeadings={false}
                onSelectedItemsChange={this.onSelectedSubSubKelompokChange}
                selectedItems={this.state.selectedSubSubKelompok}
                showChips={false}
                loading={this.state.loadingSubSubKelompok}
              />
            </View>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <TouchableOpacity
              style={[styles.buttonContainer, styles.tombol]}
              onPress={() => this.fetchListAsset()}
            >
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between"
                }}
              >
                <Icon
                  type="FontAwesome"
                  name="list"
                  style={{ color: "white" }}
                />
                <Text style={{ marginLeft: 10, color: "white", fontSize: 15 }}>
                  Daftar Aset
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      {/* </Container> */}

      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    paddingBottom: 20,
    paddingRight: 20,
    paddingLeft: 20
  },
  buttonContainer: {
    height: 40,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 5,
    width: 300,
    borderRadius: 30,
    backgroundColor: "transparent"
  },
  tombol: {
    backgroundColor: "#1f60ed",
    shadowColor: "#808080",
    shadowOffset: {
      width: 0,
      height: 9
    },
    shadowOpacity: 0.5,
    shadowRadius: 12.35,

    elevation: 19
  },
  dropdown: {
    borderWidth:3,
    borderColor:"gray",
    backgroundColor: "white",
    height: 45,
    borderRadius: 20,
    justifyContent: "center",
    paddingTop: 3,
    margin: 10,
    shadowOffset: {
      width: 0,
      height: 9
    },
    shadowColor: "black",
    shadowOpacity: 0.5,
    shadowRadius: 12.35,
    elevation: 19
  },
});

export default InputSearchScreen;
