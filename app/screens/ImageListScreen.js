import React, { Component } from "react";
import {
  Alert,
  Platform,
  View,
  ScrollView,
  StyleSheet,
  TextInput,
  Text,
  StatusBar,
  SafeAreaView,
  TouchableOpacity
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { sliderWidth, itemWidth } from "./styles/SliderEntry.style";
import SliderEntry from "./components/SliderEntry";
import styles, { colors } from "./styles/index.style";
import { Container, Fab, Icon } from "native-base";
import Headers from "./Headers";
// import Loader from "./../Loader";
import Loader from "./Helper/Loader";
import { Dropdown } from "react-native-material-dropdown";
import Api from "../Api";
import CameraView from "./CameraScreen";
import Modal from "react-native-modal";

const SLIDER_1_FIRST_ITEM = 1;

export default class ImageListScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      cameraVisible: false,
      isMonitoring: false,
      simpanMonitoring: false,
      carouselItems: [],
      slider1ActiveSlide: SLIDER_1_FIRST_ITEM,
      listKondisi: [
        {
          label: "Baik",
          value: 1
        },
        {
          label: "Rusak Ringan",
          value: 2
        },
        {
          label: "Rusak Berat",
          value: 3
        }
      ],
      kondisi: null,
      inputKondisi: null,
      keterangan: "",
      inputKeterangan: "",
      visibleModal: false
    };

    this.t = setInterval(() => {
    }, 1000);
  }

  async componentDidMount() {
    //Here is the Trick
    const { navigation } = this.props;
    this.setState({
      isMonitoring: this.props.navigation.state.params.isMonitoring
    });
    //Adding an event listner om focus
    //So whenever the screen will have focus it will set the state to zero
    this.focusListener = navigation.addListener("didFocus", () => {
      this.populateImages();
    });
    // this.state.loading = false;
  }

  componentWillUnmount() {
    // Remove the event listener before removing the screen
    this.focusListener.remove();
    clearTimeout(this.t);
  }

  populateImages() {
    const {
      kd_lokasi,
      kd_brg,
      nama_aset,
      no_aset,
      kondisi,
      keterangan
    } = this.props.navigation.state.params.rincianAset;

    this.setState({
      loading: true,
      kondisi: kondisi,
      inputKondisi: kondisi,
      keterangan: keterangan
    });

    let apiUrl =
      global.urlServer +
      "/api-android/get-image-list/?kodeUakpb=" +
      kd_lokasi +
      "&kodeBarang=" +
      kd_brg +
      "&nup=" +
      no_aset;

    let options = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    };

    fetch(apiUrl, options)
      .then(response => response.json())
      .then(response => {
        let listData = response.map(items => {
          let urlImage =
            global.urlServer +
            "/api-android/get-foto-aset/?kodeUakpb=" +
            kd_lokasi +
            "&kodeBarang=" +
            kd_brg +
            "&nup=" +
            no_aset +
            "&fileName=" +
            items.filename +
            "&timeAccess=" +
            new Date().getTime();

          return {
            id: items.id,
            title: nama_aset,
            tglFoto: items.tanggal_foto,
            subtitle: "Diupload oleh: " + items.diupload_oleh,
            illustration: urlImage,
            captionFoto: items.caption,
            navigation: this.props.navigation
          };
        });
        if (listData.length <= 0) {
          listData = [
            {
              id: 0,
              title: nama_aset,
              tglFoto: null,
              subtitle: "Foto belum ada",
              illustration:
                global.urlServer +
                "/api-android/get-foto-aset/?kodeUakpb=" +
                kd_lokasi +
                "&kodeBarang=" +
                kd_brg +
                "&nup=" +
                no_aset +
                "&fileName=default&timeAccess=" +
                new Date().getTime(),
              navigation: this.props.navigation
            }
          ];
        }
        this.setState({
          carouselItems: listData,
          loading: false
        });
      })
      .catch(function(error) {
        Alert.alert(
          "Terjadi Kesalahan",
          "Alamat server keliru atau tidak ada koneksi. \n" + error.message,
          [
            {
              text: "OK",
              onPress: () => {
                this.setState({ loading: false });
              }
            }
          ],
          { cancelable: false }
        );
      });
  }

  _renderItemWithParallax({ item, index }, parallaxProps) {
    return (
      <SliderEntry
        data={item}
        even={(index + 1) % 2 === 0}
        parallax={true}
        parallaxProps={parallaxProps}
        // navigation= {this.props.navigation}
      />
    );
  }

  renderKondisi(param) {
    switch (parseInt(param)) {
      case 1:
        return "Baik";
      case 2:
        return "Rusak Ringan";
      case 3:
        return "Rusak Berat";
      default:
        return "Tidak Diketahui";
    }
  }

  fetchSimpanKondisiBarang = () => {
    const {
      kd_lokasi,
      kd_brg,
      no_aset
    } = this.props.navigation.state.params.rincianAset;
    this.setState({
      loading: true,
      visibleModal: false
    });

    if (global.userData.kodesatker != kd_lokasi) {
      Alert.alert(
        "Izin Ditolak",
        "Anda tidak berhak mengubah kondisi aset ini",
        [
          {
            text: "OK",
            onPress: () =>
              this.setState({
                loading: false
              })
          }
        ],
        { cancelable: false }
      );
      return;
    }
    Api.fetchSimpanKondisiBarang(
      kd_brg,
      no_aset,
      kd_lokasi,
      global.userData.id,
      this.state.inputKondisi,
      this.state.inputKeterangan,
      (onSuccess = response => {
        if (response != null)
          Alert.alert(
            "Informasi",
            "Data berhasil disimpan",
            [
              {
                text: "OK",
                onPress: () => {
                  this.setState({
                    loading: false
                  });
                  if (this.state.simpanMonitoring) {
                    this.props.navigation.state.params.isMonitoring = false;
                    this.props.navigation.pop();
                  } else {
                    this.setState({
                      kondisi:this.state.inputKondisi,
                      keterangan:this.state.inputKeterangan,
                    })
                    this.props.navigation.state.params.rincianAset.kondisi=this.state.inputKondisi;
                    this.props.navigation.state.params.rincianAset.keterangan=this.state.inputKeterangan;
                  }
                }
              }
            ],
            { cancelable: false }
          );
      }),
      (onError = error => {
        Alert.alert(
          "Proses Gagal",
          "Data tidak berhasil disimpan",
          [
            {
              text: "OK",
              onPress: () =>
                this.setState({
                  loading: false
                })
            }
          ],
          { cancelable: false }
        );
        this.setState({
          loading: false
        });
      })
    );
  };

  fetchUpdateMonitoring = () => {
    const {
      kd_lokasi,
      id
    } = this.props.navigation.state.params.rincianAset;
    this.setState({
      loading: true,
      visibleModal: false
    });

    if (global.userData.kodesatker != kd_lokasi) {
      Alert.alert(
        "Izin Ditolak",
        "Anda tidak berhak mengubah kondisi aset ini",
        [
          {
            text: "OK",
            onPress: () =>
              this.setState({
                loading: false
              })
          }
        ],
        { cancelable: false }
      );
      return;
    }
    Api.fetchUpdateMonitoring(
      id,
      (onSuccess = response => {
        if (response != null)
          Alert.alert(
            "Informasi",
            "Data berhasil disimpan",
            [
              {
                text: "OK",
                onPress: () => {
                  this.setState({
                    loading: false
                  });
                  if (this.state.simpanMonitoring) {
                    this.setState({simpanMonitoring:false});
                    this.setState({isMonitoring:false});
                  }
                }
              }
            ],
            { cancelable: false }
          );
      }),
      (onError = error => {
        Alert.alert(
          "Proses Gagal",
          "Data tidak berhasil disimpan",
          [
            {
              text: "OK",
              onPress: () =>
                this.setState({
                  loading: false
                })
            }
          ],
          { cancelable: false }
        );
        this.setState({
          loading: false
        });
      })
    );
  };

  ubahKondisi(value) {
    this.setState({
      inputKondisi: value
    });
  }

  slideImage() {
    const {
      nama_aset,
      tgl_perlh,
      merk_type,
      nilaibmn
    } = this.props.navigation.state.params.rincianAset;

    const { slider1ActiveSlide } = this.state;

    return (
      <View style={styles.exampleContainer}>
        <Text style={styles.title}>
          {nama_aset} (Nilai: {nilaibmn})
        </Text>
        <Text style={styles.subtitle}>
          Merk/ Type: {merk_type} | Tgl Peroleh: {tgl_perlh}
        </Text>

        <Carousel
          ref={c => (this._slider1Ref = c)}
          data={this.state.carouselItems}
          renderItem={this._renderItemWithParallax}
          sliderWidth={sliderWidth}
          itemWidth={itemWidth}
          hasParallaxImages={true}
          firstItem={SLIDER_1_FIRST_ITEM}
          inactiveSlideScale={0.94}
          inactiveSlideOpacity={0.7}
          // inactiveSlideShift={20}
          containerCustomStyle={styles.slider}
          contentContainerCustomStyle={styles.sliderContentContainer}
          loop={true}
          loopClonesPerSide={2}
          autoplay={true}
          autoplayDelay={500}
          autoplayInterval={3000}
          onSnapToItem={index => this.setState({ slider1ActiveSlide: index })}
        />
        <Pagination
          dotsLength={this.state.carouselItems.length}
          activeDotIndex={slider1ActiveSlide}
          containerStyle={styles.paginationContainer}
          dotColor={"rgba(255, 255, 255, 0.92)"}
          dotStyle={styles.paginationDot}
          inactiveDotColor={colors.black}
          inactiveDotOpacity={0.4}
          inactiveDotScale={0.6}
          carouselRef={this._slider1Ref}
          tappableDots={!!this._slider1Ref}
        />
      </View>
    );
  }

  get gradient() {
    return (
      <LinearGradient
        colors={[colors.background1, colors.background2]}
        startPoint={{ x: 1, y: 0 }}
        endPoint={{ x: 0, y: 1 }}
        style={styles.gradient}
      />
    );
  }

  fetchCamera = cameraVisible => {
    this.setState({
      cameraVisible: cameraVisible
    });
    this.populateImages();
  };

  _renderModalContent = () => (
    <View style={modalStyles.modalContent}>
      {this.gradient}
      <View
        style={{
          height: 50,
          width: "100%",
          backgroundColor: "#034a91",
          justifyContent: "space-between",
          alignItems: "center"
        }}
      >
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "space-between"
          }}
        >
          <View
            style={{
              flex: 1,
              padding: 10,
              justifyContent: "center",
              alignItems: "flex-start"
            }}
          >
            <Text
              style={{
                color: "white",
                fontWeight: "bold"
              }}
            >
              Ubah Kondisi Aset
            </Text>
          </View>

          <View
            style={{
              flex: 1,
              padding: 5,
              justifyContent: "center",
              alignItems: "flex-end"
            }}
          >
            <TouchableOpacity
              style={[
                {
                  height: 40,
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                  marginBottom: 5,
                  width: 40,
                  borderRadius: 30,
                  backgroundColor: "transparent"
                },
                {
                  backgroundColor: "red",
                  shadowColor: "#808080",
                  shadowOffset: {
                    width: 0,
                    height: 9
                  },
                  shadowOpacity: 0.5,
                  shadowRadius: 12.35,

                  elevation: 19
                }
              ]}
              onPress={() => this.setState({ visibleModal: false })}
            >
              <Icon
                type="FontAwesome"
                name="close"
                style={{ color: "white" }}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View
        style={{
          justifyContent: "center",
          alignItems: "center",
          padding: 5,
          margin: 5
        }}
      >
        <Text>Kondisi</Text>

        <Dropdown
          data={this.state.listKondisi}
          containerStyle={{
            width: 160,
            height: 45,
            justifyContent: "center",
            backgroundColor: "white",
            marginVertical: 6,
            paddingBottom: 15,
            paddingHorizontal: 4,
            borderRadius: 20,
            borderWidth: 2,
            borderColor: "gray"
          }}
          pickerStyle={{
            backgroundColor: "white"
          }}
          value={this.renderKondisi(this.state.kondisi)}
          onChangeText={value => this.ubahKondisi(value)}
        />

        <Text>Keterangan</Text>

        <View
          style={{
            borderBottomColor: "#F5FCFF",
            backgroundColor: "#FFFFFF",
            borderRadius: 30,
            borderBottomWidth: 1,
            width: 300,
            height: 45,
            marginBottom: 20,
            flexDirection: "row",
            alignItems: "center",

            shadowColor: "#808080",
            shadowOffset: {
              width: 0,
              height: 2
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,

            elevation: 5
          }}
        >
          <TextInput
            style={{
              height: 45,
              marginLeft: 16,
              borderBottomColor: "#FFFFFF",
              flex: 1
            }}
            placeholder="Keterangan"
            underlineColorAndroid="transparent"
            onChangeText={inputKeterangan => this.setState({ inputKeterangan })}
          />
        </View>

        <TouchableOpacity
          style={[
            {
              height: 40,
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
              marginBottom: 5,
              width: 200,
              borderRadius: 30,
              backgroundColor: "transparent"
            },
            {
              backgroundColor: "#032373",
              shadowColor: "#808080",
              shadowOffset: {
                width: 0,
                height: 9
              },
              shadowOpacity: 0.5,
              shadowRadius: 12.35,

              elevation: 19
            }
          ]}
          onPress={() => this.fetchSimpanKondisiBarang()}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between"
            }}
          >
            <Icon type="FontAwesome" name="save" style={{ color: "white" }} />
            <Text style={{ marginLeft: 10, color: "white", fontSize: 15 }}>
              Simpan
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );

  render() {
    if (this.state.loading) {
      return <Loader />;
    }
    const { rincianAset } = this.props.navigation.state.params;
    if (this.state.cameraVisible) {
      return (
        <CameraView fetchCamera={this.fetchCamera} rincianAset={rincianAset} />
      );
    }
    if (this.state.zoomVisible) {
      this.setState({ zoomVisible: false });
      this.populateImages();
    }
    const example1 = this.slideImage();

    return (
      <Container>
        <Headers
          title={"Daftar Gambar Aset"}
          navigation={this.props.navigation}
          handlePostClick={this.handlePostClick}
        />
        <ScrollView>
          <SafeAreaView style={styles.safeArea}>
            <View style={styles.container}>
              <StatusBar
                translucent={true}
                backgroundColor={"rgba(0, 0, 0, 0.3)"}
                barStyle={"light-content"}
              />
              {this.gradient}
              {example1}
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  padding: 5
                }}
              >
                <View
                  style={{
                    backgroundColor: "rgba(255,255,255,0.5)",
                    borderTopColor: "white",
                    borderTopWidth: 3,
                    width: "60%",
                    justifyContent: "center",
                    alignItems: "center",
                    padding: 10,
                    margin: 10
                  }}
                >
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      padding: 10,
                      margin: 5,
                      borderColor: "white",
                      borderBottomWidth: 1
                    }}
                  >
                    <View
                      style={{
                        flex: 1,
                        justifyContent: "center"
                      }}
                    >
                      <Text>Kondisi</Text>
                    </View>
                    <View
                      style={{
                        flex: 2,
                        justifyContent: "center"
                      }}
                    >
                      <Text
                        style={{
                          backgroundColor: "transparent",
                          color: "blue",
                          fontSize: 15,
                          fontStyle: "italic"
                        }}
                      >
                        {this.renderKondisi(this.state.kondisi)}
                      </Text>
                    </View>
                  </View>

                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      padding: 10,
                      margin: 5,
                      borderColor: "white",
                      borderBottomWidth: 1
                    }}
                  >
                    <View
                      style={{
                        flex: 1,
                        justifyContent: "center"
                      }}
                    >
                      <Text>Keterangan</Text>
                    </View>
                    <View
                      style={{
                        flex: 2,
                        justifyContent: "center"
                      }}
                    >
                      <Text
                        style={{
                          backgroundColor: "transparent",
                          color: "blue",
                          fontSize: 15,
                          fontStyle: "italic"
                        }}
                      >
                        {this.state.keterangan}
                      </Text>
                    </View>
                  </View>

                </View>

                <TouchableOpacity
                  style={[
                    {
                      height: 40,
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center",
                      marginBottom: 5,
                      width: 200,
                      borderRadius: 30,
                      backgroundColor: "transparent"
                    },
                    {
                      backgroundColor: "#032373",
                      shadowColor: "#808080",
                      shadowOffset: {
                        width: 0,
                        height: 9
                      },
                      shadowOpacity: 0.5,
                      shadowRadius: 12.35,

                      elevation: 19
                    }
                  ]}
                  onPress={() => this.setState({ 
                    inputKondisi:this.state.kondisi,
                    inputKeterangan:"",
                    visibleModal: true 
                  })} 
                >
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "space-between"
                    }}
                  >
                    <Icon
                      type="FontAwesome"
                      name="edit"
                      style={{ color: "white" }}
                    />
                    <Text
                      style={{ marginLeft: 10, color: "white", fontSize: 15 }}
                    >
                      Ubah Kondisi Aset
                    </Text>
                  </View>
                </TouchableOpacity>
                {this.state.isMonitoring ? (
                  <TouchableOpacity
                    style={[
                      {
                        height: 40,
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center",
                        marginBottom: 5,
                        width: 200,
                        borderRadius: 30,
                        backgroundColor: "transparent"
                      },
                      {
                        backgroundColor: "#032373",
                        shadowColor: "#808080",
                        shadowOffset: {
                          width: 0,
                          height: 9
                        },
                        shadowOpacity: 0.5,
                        shadowRadius: 12.35,

                        elevation: 19
                      }
                    ]}
                    onPress={() => {
                      this.setState({ simpanMonitoring: true });
                      this.fetchUpdateMonitoring();
                    }}
                  >
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "space-between"
                      }}
                    >
                      <Icon
                        type="FontAwesome"
                        name="check"
                        style={{ color: "white" }}
                      />
                      <Text
                        style={{ marginLeft: 10, color: "white", fontSize: 15 }}
                      >
                        Sudah Dimonitoring
                      </Text>
                    </View>
                  </TouchableOpacity>
                ) : null}
                <Modal isVisible={this.state.visibleModal}>
                  {this._renderModalContent()}
                </Modal>
              </View>
            </View>
            <View>
              <Fab
                style={{ backgroundColor: "#1e88e5" }}
                position="bottomRight"
                onPress={this._takePhoto}
              >
                <Icon type="FontAwesome" name="camera" />
              </Fab>
            </View>
          </SafeAreaView>
        </ScrollView>
      </Container>
    );
  }

  _takePhoto = async () => {
    const { kd_lokasi } = this.props.navigation.state.params.rincianAset;
    if (global.userData.kodesatker != kd_lokasi) {
      Alert.alert(
        "Izin Ditolak",
        "Anda tidak berhak menambah foto aset",
        [
          {
            text: "OK",
            onPress: () =>
              this.setState({
                loading: false
              })
          }
        ],
        { cancelable: false }
      );
      return;
    }
    this.setState({ cameraVisible: true });
  };
}

const modalStyles = StyleSheet.create({
  modalContent: {
    backgroundColor: "#35aaf2",
    // padding: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    borderColor: "rgba(0, 0, 0, 0.1)"
  }
});
