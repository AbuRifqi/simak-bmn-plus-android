import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";
import { ParallaxImage } from "react-native-snap-carousel";
import styles from "../styles/SliderEntry.style";
import Moment from "moment";

export default class SliderEntry extends Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
    even: PropTypes.bool,
    parallax: PropTypes.bool,
    parallaxProps: PropTypes.object,
  };

  get image() {
    const {
      data: { illustration },
      parallax,
      parallaxProps,
      even
    } = this.props;

    return parallax ? (
      <ParallaxImage
        source={{ uri: illustration }}
        containerStyle={[
          styles.imageContainer,
          even ? styles.imageContainerEven : {}
        ]}
        style={styles.image}
        parallaxFactor={0.35}
        showSpinner={true}
        spinnerColor={even ? "rgba(255, 255, 255, 0.4)" : "rgba(0, 0, 0, 0.25)"}
        {...parallaxProps}
      />
    ) : (
      <Image source={{ uri: illustration }} style={styles.image} />
    );
  }

  zoomImage = () => {
    console.log("DATA = "+JSON.stringify(this.props.data));
    this.props.data.navigation.navigate("ZoomImage", {
      name: "from parent",
      data: this.props.data,
    });
  };

  render() {
    const {
      data: {id, tglFoto, title, subtitle,captionFoto },
      even
    } = this.props;

    const uppercaseTitle = title ? (
      tglFoto==null?false:
      <Text
        style={[styles.title, even ? styles.titleEven : {}]}
        numberOfLines={2}
      >
        Tanggal Foto: {Moment(tglFoto).format("DD MMMM YYYY")}
        {/* {title.toUpperCase()} */}
      </Text>
    ) : (
      false
    );

    return (
      <View>
        <TouchableOpacity
          activeOpacity={1}
          style={styles.slideInnerContainer}
          onPress={() => this.zoomImage()}
        //   {() => {
        //     console.log(`You've clicked '${id}'`);
        //   }}
        >
          <View style={styles.shadow} />
          <View
            style={[
              styles.imageContainer,
              even ? styles.imageContainerEven : {}
            ]}
          >
            
            {this.image}
            <View
              style={[styles.radiusMask, even ? styles.radiusMaskEven : {}]}
            />
          </View>
          <View
            style={[styles.textContainer, even ? styles.textContainerEven : {}]}
          >
            {uppercaseTitle}
            <Text
              style={[styles.subtitle, even ? styles.subtitleEven : {}]}
              numberOfLines={2}
            >
              {subtitle}
            </Text>
            <Text
              style={[styles.subtitle, even ? styles.subtitleEven : {}]}
              numberOfLines={2}
            >
              Keterangan: {captionFoto}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
