import React, { Component } from "react";
import {
  Image,
  View,
  Dimensions,
  StyleSheet,
  Platform,
  Alert,
  StatusBar,
  TouchableOpacity,
  Text
} from "react-native";
import ImageZoom from "react-native-image-pan-zoom";
import {
  Fab,
  Icon,
  Button,
  Container,
  Header,
  Left,
  Right,
  Body,
  Title
} from "native-base";
import { LinearGradient } from "expo-linear-gradient";
import Headers from "./Headers";
// import Loader from "./Helper/Loader";
import Menu, { MenuItem } from "react-native-material-menu";

export default class ZoomImageScreen extends Component {
    //Menu-------------------------
    _menu = null;

    setMenuRef = ref => {
      this._menu = ref;
    };
  
    hideMenu = () => {
      this._menu.hide();
    };
  
    showMenu = () => {
      this._menu.show();
    };
    //--------------------
  
    deleteFoto(){
      const {data}=this.props.navigation.state.params;

      let apiUrl =
      global.urlServer +
      "/api-android/delete-foto-aset/?idFoto=" +data.id;

    let options = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    };
    fetch(apiUrl, options)
      .then(response => response.json())
      .then(response => {
        this.props.navigation.pop();
        // this.props.zoomVisible
      })
      .catch(function(error) {
        Alert.alert(
          "Terjadi Kesalahan",
          "Alamat server keliru atau tidak ada koneksi. \n" + error.message,
          [
            {
              text: "OK",
              onPress: () => {
                this.state.loading=false;
              }
            }
          ],
          { cancelable: false }
        );
      });
    }

  confirmDelete() {
    this.hideMenu();
    Alert.alert(
      "Konfirmasi",
      "Apakah Anda yakin ingin menghapus foto aset ini?",
      [
        {
          text: "Tidak",
          onPress: () => {
          }
        },
        {
          text: "Yakin",
          onPress: () => {
            this.deleteFoto();
          }
        }
      ],
      { cancelable: true }
    );
  }
  _renderMenu() {
    return (
      <Menu
        ref={ref => (this._menu = ref)}
        button={
          <TouchableOpacity
            onPress={() => this._menu.show()}
            style={{
              paddingHorizontal: 16,
              height: "100%",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Icon
              type="FontAwesome"
              name="ellipsis-v"
              style={{ color: "white" }}
            />
          </TouchableOpacity>
        }
      >
        <MenuItem
          onPress={() => this.confirmDelete()}
          textStyle={{ color: "#000", fontSize: 16 }}
        >
          Hapus
        </MenuItem>
      </Menu>
    );
  }

  componentWillUnmount() {
    // Remove the event listener before removing the screen
    // clearTimeout(this.t);
  }
  
  render() {
    const { data } = this.props.navigation.state.params;
    // console.log("DATA = " + JSON.stringify(data));
    return (
      <Container
        style={{
          flex: 1
        }}
      >
        <Headers
          title={data.title}
          navigation={this.props.navigation}
          menu={this._renderMenu()}
          handlePostClick={this.handlePostClick}
        />
        {/* <View>
          <View style={{flex:1,borderWidth:1}}>
          <TouchableOpacity
                    style={[styles.buttonContainer, styles.tombol]}
                    onPress={this._takePhoto}
                  >
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "space-between"
                      }}
                    >
                      <Icon
                        type="FontAwesome"
                        name="trash"
                        style={{ color: "white" }}
                      />
                      <Text
                        style={{ marginLeft: 10, color: "white", fontSize: 15 }}
                      >
                        Hapus
                      </Text>
                    </View>
                  </TouchableOpacity>
             <Fab
              style={{ backgroundColor: "red" }}
              position="topRight"
              onPress={() => this.confirmDelete()}
            >
              <Icon type="FontAwesome5" name="trash" />
            </Fab>
          </View>
          <View style={{flex:5, borderWidth:1}}> */}
        <ImageZoom
          cropWidth={Dimensions.get("window").width}
          cropHeight={Dimensions.get("window").height}
          imageWidth={400}
          imageHeight={400}
        >
          <Image
            style={{ width: 400, height: 400 }}
            source={{
              uri: data.illustration,
              headers: { Pragma: "no-cache" },
              cache: "reload"
            }}
            // style={{ flex: 1, height: 200, width: 200, resizeMode: "contain" }}
            //  source={{uri:this.state.uriFoto}}
          />
        </ImageZoom>
        {/* </View>
        </View> */}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: "#031b57",
    ...Platform.select({
      android: {
        marginTop: StatusBar.currentHeight
      }
    }),
    padding: 0,
    margin: 0
  },
  buttonContainer: {
    height: 40,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 5,
    width: 300,
    borderRadius: 30,
    backgroundColor: "transparent"
  },
  tombol: {
    backgroundColor: "#00b5ec",
    shadowColor: "#808080",
    shadowOffset: {
      width: 0,
      height: 9
    },
    shadowOpacity: 0.5,
    shadowRadius: 12.35,

    elevation: 19
  }
});
