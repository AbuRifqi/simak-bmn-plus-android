import Constants from "expo-constants";
import React, { Component } from "react";
import {
  Platform,
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
  Alert,
  InteractionManager,
  dimensions,
  StatusBar,
  KeyboardAvoidingView
} from "react-native";
import { FontAwesome } from "@expo/vector-icons";
import * as Location from "expo-location";
import * as Permissions from "expo-permissions";
import { Camera } from "expo-camera";
import DialogInput from "react-native-dialog-input";
import { Container, Header, Left, Body, Right,Icon,Title,Button } from "native-base";
import { LinearGradient } from "expo-linear-gradient";

// import ApolloClient from "apollo-client";
// import { HttpLink } from 'apollo-link-http';
// import { InMemoryCache } from 'apollo-cache-inmemory';

import Loader from "./Helper/Loader";
import CameraLoader from "./Helper/CameraLoader";
import moment from "moment";

export default class CameraScreen extends Component {
  state = {
    fotoCamera: null,
    captionFoto: "",
    location: null,
    loading: false,
    cameraLoading: false,
    hasCameraPermission: null,
    type: Camera.Constants.Type.back,
    photo: null,
    url: "",
    pictureSize: "",
    errorMessage:""
  };

  constructor() {
    super();
    this.onCameraReady = this.onCameraReady.bind(this);
  }

  _cameraPermissionAsync = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === "granted" });
  };

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== "granted") {
      this.setState({
        errorMessage: "Akses lokasi tidak diizinkan"
      });
    }

    let location = await Location.getCurrentPositionAsync({});
    this.setState({ location });
  };

  /* 
  function pickRatio_calcDimensions(ratios: string[]): [string, ViewStyle] {
    const ratio = ratios[getIndexOfMinRatio(ratios)]
    const [height, width] = getDimensionsFromRatio(ratio)
    const dimensions: ViewStyle = { width: windowWidth, height: (windowWidth * height) / width }
    return [ratio, dimensions]
  } */

  async onCameraReady() {
    /*  this.setState({pictureSizeText: "querying picture size values"});
     var sizes = await this.camera.getAvailablePictureSizesAsync("1:1");
     console.log("Received sizes: " + JSON.stringify(sizes))
     if (sizes && sizes.length && sizes.length > 0){
       
       console.log("current picture size: " + this.camera.pictureSize)
       console.log("going to change sizes into this: " + sizes[0])
       this.camera.pictureSize = sizes[0];
       console.log("new picture size: " + this.camera.pictureSize)
      this.setState({pictureSizeText: "changed picture size to: " + this.camera.pictureSize + " (lowest)"});

     } */
    if (Platform.OS === "android") {
      /* InteractionManager.runAfterInteractions(async () => {
          const ratios = await this.camera.getSupportedRatiosAsync()
          //const [ratio, dimensions] = pickRatio_calcDimensions(ratios)
          console.log(ratios)
          console.log(ratios[0])
          const sizes = await this.camera.getAvailablePictureSizesAsync("3:4")
          this.setState({ pictureSize: sizes[0], cameraStyle: dimensions })
          //this.state.pictureSize = sizes[0];
          console.log({ sizes })
        }) */
    }
  }

  async uploadImageAsync() {
    const {kd_lokasi, kd_brg, no_aset,nama_aset}= this.props.rincianAset;
    let apiUrl = globalThis.urlServer + "/api-android/upload-image-android/";
  
    let fileType = "png"; 
    let formData = new FormData();
    let uri = this.state.fotoCamera.uri;
    let loc = this.state.location;
    let caption = this.state.captionFoto;
    formData.append("bmnimage", {
      uri,
      name: `${nama_aset}.${fileType}`,
      type: `image/${fileType}`,
      timestamp: loc.timestamp,
      mocked: loc.mocked,
      coords: loc.coords
    });
    formData.append("kodesatker", kd_lokasi);
    formData.append("kode_bmn", kd_brg);
    formData.append("nup", no_aset);
    let dateSerial =
      new Date().getFullYear() +
      "/" +
      (new Date().getMonth() + 1) +
      "/" +
      new Date().getDate() +
      " " +
      new Date().getHours() +
      ":" +
      new Date().getMinutes() +
      ":" +
      new Date().getSeconds();
  
    formData.append("tanggal_foto", dateSerial);
    console.log("CAPTION SIMPAN = "+caption);
    formData.append("caption", caption);
  
    formData.append("meta_data", JSON.stringify(loc));
    formData.append("username", global.userName);
  
    console.log ("FORM DATA ="+ JSON.stringify(formData));
    let options = {
      method: "POST",
      body: formData,
      headers: {
        Accept: "application/json",
        "Content-Type": "multipart/form-data"
      }
    };
  
    console.log("SIMPAN FOTO = " + apiUrl);
    return fetch(apiUrl, options);
  }

  cameraChange = () => {
    this.setState({
      imageuri: "",
      url: "",
      type:
        this.state.type === Camera.Constants.Type.back
          ? Camera.Constants.Type.front
          : Camera.Constants.Type.back
    });
  };

  snap = async () => {
    this.setState({ cameraLoading: true });
    this.setState({ fotoCamera: null });
    if (this.camera) {
      const options = { quality: 0.4, base64: true };
      let photo = await this.camera.takePictureAsync(options);
      this.setState({ cameraLoading: false });
      if (photo) {
        this.setState({ fotoCamera: photo });
      }
    }
    this.setState({ cameraLoading: false });
  };

  _locationAndCamera = () => {
    if (Platform.OS === "android" && !Constants.isDevice && false) {
      this.setState({
        errorMessage:
          "Fungsi ini tidak bekerja di emulator, coba terapkan di perangkat android Anda!"
      });
    } else {
      this._getLocationAsync();
    }
    this._cameraPermissionAsync();
  };

  simpanFoto=()=>{
    this.setState({loading:true});
    this.uploadImageAsync().then((response)=>{
      console.log("HASIL POST = "+JSON.stringify(response));
      this.setState({loading:false});
      this.props.fetchCamera(false);
    });
  }

  _handleErrorGQL = err => {
    console.log(JSON.stringify(err));
    let errMsg = "";
    if (err.graphQLErrors != null && err.graphQLErrors.length > 0) {
      errMsg = err.graphQLErrors[0].message;
    } else {
      if (err.message) errMsg = err.message;
      else errMsg = JSON.stringify(err);
    }
    this.setState({
      loading: false
    });

    return errMsg;
  };

  componentWillMount() {
    this.state.devid = Expo.Constants.deviceId;
    console.log ("RINCIAN ASET = " + JSON.stringify(this.props.rincianAset) );
    this._locationAndCamera();
  }
  render() {
    if (this.state.loading) {
      return <Loader />;
    }

    const { hasCameraPermission } = this.state;
    if (hasCameraPermission === null) {
      return <View />;
    } else if (hasCameraPermission === false) {
      return (
        <View>
          <Text>Kamera tidak dapat diakses</Text>
        </View>
      );
    } else {
      return (
        <View
          style={{
            flex: 1
          }}
        >
          <Header style={head.container} androidStatusBarColor="#1e88e5">
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              locations={[0.2, 1]}
              style={{
                flex: 1,
                flexDirection: "row"
              }}
              colors={["#031b57", "#7a8fc2"]}
            >
              <Left>
                <Button
                  transparent
                  onPress={() => {
                    this.props.fetchCamera(false);
                  }}
                >
                  <Icon name="arrow-back" />
                </Button>
              </Left>
              <Body style={{ left: 60 }}>
                <Title>Ambil Foto Aset</Title>
              </Body>
            </LinearGradient>
          </Header>
          <KeyboardAvoidingView
          style={styles.container}
          behavior="padding"
          enabled
        >
          {/* <View style={styles.container}> */}
            {this.state.cameraLoading == true ? <CameraLoader /> : null}
            <View style={styles.cameraview}>
              {this.state.fotoCamera != null ? (
                <Image
                  source={{
                    uri: this.state.fotoCamera.uri
                  }}
                  style={styles.uploadedImage}
                  resizeMode="center"
                />
              ) : (
                <Camera
                  pictureSize="320x240"
                  onCameraReady={this.onCameraReady}
                  style={styles.camera}
                  type={this.state.type}
                  ref={ref => {
                    this.camera = ref;
                  }}
                >
                  <View style={styles.camerabuttonview}>
                    <TouchableOpacity
                      style={styles.cameraButtons}
                      onPress={this.cameraChange}
                    >
                      <Text
                        style={{
                          fontSize: 18,
                          marginBottom: 10,
                          color: "white"
                        }}
                      >
                        Flip
                      </Text>
                    </TouchableOpacity>
                  </View>
                </Camera>
              )}
            </View>
            <View style={styles.inputContainer}>
                  <TextInput
                    style={styles.inputs}
                    placeholder="Keterangan"
                    underlineColorAndroid="transparent"
                    onChangeText={captionFoto => this.setState({ captionFoto })}
                  />
                </View>
            {this.state.fotoCamera == null ? (
                <FontAwesome.Button name="camera" size={25} onPress={this.snap}>
                  Ambil Foto
                </FontAwesome.Button>
              ) : (
                <View>
                <FontAwesome.Button
                  name="close"
                  size={25}
                  backgroundColor="#ff0000"
                  onPress={this.snap}
                >
                  Ambil Ulang Foto
                </FontAwesome.Button>
                <FontAwesome.Button name="save" size={25} onPress={this.simpanFoto}>
                  Simpan
                </FontAwesome.Button>
                </View>
              )}
          {/* </View> */}
          </KeyboardAvoidingView>
        </View>
      );
    }
  }
}

const head = StyleSheet.create({
  container: {
    backgroundColor: "#031b57",
    ...Platform.select({
      android: {
        marginTop: StatusBar.currentHeight
      }
    })
  }
});
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingTop: Constants.statusBarHeight,
    backgroundColor: "#ecf0f1"
  },
  cameraview: {
    height: "50%",
    width: "90%",
    backgroundColor: "white",
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center"
  },
  camera: {
    height: "95%",
    width: "95%",
    backgroundColor: "white",
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center"
  },
  camerabuttonview: {
    height: "100%",
    backgroundColor: "transparent"
  },
  cameraButtons: {
    borderColor: "#fff",
    borderWidth: 2,
    padding: 10,
    borderRadius: 5,
    margin: 5
  },
  captureButtonView: {
    height: 200
  },
  uploadedImage: {
    flex: 1,
    height: "100%",
    width: "100%",
    resizeMode: "cover"
  },
  inputContainer: {
    borderBottomColor: "#F5FCFF",
    backgroundColor: "#FFFFFF",
    borderRadius: 30,
    borderBottomWidth: 1,
    width: 300,
    height: 45,
    marginBottom: 20,
    flexDirection: "row",
    alignItems: "center",

    shadowColor: "#808080",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    borderBottomColor: "#FFFFFF",
    flex: 1
  },
});
