import React from 'react';
import { StyleSheet } from 'react-native';
import AnimatedLoader from "react-native-animated-loader";

export default class CameraLoader extends React.Component {
  constructor(props) {
    super(props);
    this.state = { visible: true };
  }

  render() {
    const { visible } = this.state;
    return (
      <AnimatedLoader
        visible={visible}
        overlayColor="rgba(255,255,255,0.75)"
        source={require("../../../assets/animations/4309-take-photo")}
        animationStyle={styles.lottie}
        speed={1.5}
      />
    );
  }
}

const styles = StyleSheet.create({
  lottie: {
    width: 200,
    height: 200
  }
});