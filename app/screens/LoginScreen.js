import React, { Component } from "react";
import {
  ImageBackground,
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableOpacity,
  Image,
  Alert,
  KeyboardAvoidingView
} from "react-native";
// import Loader from "./../Loader";
import Loader from "./Helper/Loader";
import DialogInput from "react-native-dialog-input";
import { H1, Icon } from "native-base";
import fetch from "react-native-fetch-polyfill";
import Storage from "../Storage";

export default class LoginScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username:"",//"setama1@bppt-test.go.id",
      password:"", //"admin",
      loading: false,
      dialogVisible: false,
      serverAddress: null
    };
  }

  async componentDidMount() {
    this.setState({ loading: true });
    await this.getURLServer().then(respon => {
      global.urlServer = respon;
      // global.urlServer='http://202.46.3.61';
      // global.urlServer='http://192.168.88.13:8013/bppt-bmn/web';
      if (global.urlServer == null || global.urlServer == "") {
        this.setState({
          dialogVisible: true
        });
      }
      this.setState({
        serverAddress: global.urlServer,
        loading: false
      });
    });
  }

  render() {
    if (this.state.loading) {
      return <Loader />;
    }
    return (
      <ImageBackground
        style={{ flex: 1 }}
        //We are using online image to set background
        source={require("./../../assets/backgrounds/login.jpg")}
      >
        <KeyboardAvoidingView
          style={styles.container}
          behavior="padding"
          enabled
        >
          <View>
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                padding: 10
              }}
            >
              <Image
                source={require("./../../assets/bppt-logo.png")}
                style={{
                  flex: 1,
                  width: "70%",
                  resizeMode: "contain",
                  alignSelf: "center",
                  padding: 5
                }}
              />

              <H1
                style={{
                  alignItems: "center",
                  padding: 5,
                  textAlign: "center",
                  color: "#031645"
                }}
              >
                Login
              </H1>
            </View>

            <View
              style={{
                flex: 1,
                justifyContent: "center"
              }}
            >
              <View style={styles.container}>
                <View style={styles.inputContainer}>
                  <TextInput
                    style={styles.inputs}
                    placeholder="Username"
                    underlineColorAndroid="transparent"
                    onChangeText={username => this.setState({ username })}
                  />
                </View>

                <View style={styles.inputContainer}>
                  <TextInput
                    style={styles.inputs}
                    placeholder="Password"
                    secureTextEntry={true}
                    underlineColorAndroid="transparent"
                    onChangeText={password => this.setState({ password })}
                  />
                </View>
                <TouchableOpacity
                  style={[styles.buttonContainer, styles.loginButton]}
                  onPress={() => this.onClickListener("login")}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "space-between"
                    }}
                  >
                    <Icon
                      type="FontAwesome"
                      style={styles.loginText}
                      name="sign-in"
                    />
                    <Text style={styles.loginText}>Login</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.buttonContainer, styles.settingButton]}
                  onPress={this.showDialog}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "space-between"
                    }}
                  >
                    <Icon
                      type="FontAwesome"
                      style={styles.loginText}
                      name="cogs"
                    />
                    <Text style={styles.loginText}>Atur URL Server</Text>
                  </View>
                </TouchableOpacity>

                <DialogInput
                  isDialogVisible={this.state.dialogVisible}
                  title="Atur URL Server"
                  message={"Input URL Server : "}
                  hintInput={"Alamat URL Server"}
                  submitInput={urlServer => {
                    this.handleURLServer(urlServer);
                  }}
                  closeDialog={() => {
                    this.setState({ dialogVisible: false });
                  }}
                ></DialogInput>
              </View>
              <Text
                style={{
                  textAlign: "center",
                  justifyContent: "flex-end",
                  alignItems: "center",
                  color: "blue"
                }}
              >
                Server : {this.state.serverAddress}
              </Text>
            </View>
          </View>
        </KeyboardAvoidingView>
      </ImageBackground>
    );
  }

  showDialog = () => {
    // this.getURLServer().then(respon => (global.urlServer = respon));
    this.setState({ dialogVisible: true });
  };

  handleURLServer = urlServer => {
    this.setState({
      dialogVisible: false,
      loading: true
    });
    this.saveURLServer("http://" + urlServer).then(response => {
      this.setState({
        serverAddress: response,
        loading: false
      });
    });
  };

  async saveURLServer(alamatURL) {
    console.log("ALAMAT URL = " + alamatURL);
    return new Promise(function(resolve, reject) {
      fetch(alamatURL)
        .then(function(response) {
          console.log("RESPON : " + JSON.stringify(response));
          if (response.ok) {
            global.urlServer = alamatURL;
            Storage.saveData("@urlserver", alamatURL);
            resolve(global.urlServer);
          } else {
            Alert.alert(
              "Perhatian",
              "Alamat server keliru atau tidak ada koneksi",
              [{ text: "OK", onPress: () => resolve("") }],
              { cancelable: false }
            );
          }
        })
        .catch(function(error) {
          Alert.alert(
            "Terjadi Kesalahan",
            "Alamat server keliru atau tidak ada koneksi. \n" + error.message,
            [{ text: "OK", onPress: () => resolve("") }],
            { cancelable: false }
          );

          // reject(new Error(`Unable to retrieve events.\n${error.message}`));
        });
    });
  }

  async getURLServer() {
    return await Storage.getData("@urlserver");
  }

  onClickListener = viewId => {
    if (viewId == "login") {
      if (this.state.username == "" || this.state.password == "") {
        return Alert.alert(
          "Perhatian",
          "Nama user atau password tidak boleh kosong",
          [{ text: "OK", onPress: () => console.log("OK Pressed") }],
          { cancelable: false }
        );
      }
      this.props.fetchLogin(this.state.username, this.state.password);
    } else {
    }
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    padding: 5
  },
  inputContainer: {
    borderBottomColor: "#F5FCFF",
    backgroundColor: "#FFFFFF",
    borderRadius: 30,
    borderBottomWidth: 1,
    width: 300,
    height: 45,
    marginBottom: 20,
    flexDirection: "row",
    alignItems: "center",

    shadowColor: "#808080",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    borderBottomColor: "#FFFFFF",
    flex: 1
  },
  buttonContainer: {
    height: 45,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
    width: 300,
    borderRadius: 30,
    backgroundColor: "transparent"
  },
  loginButton: {
    backgroundColor: "#032373",
    shadowColor: "#808080",
    shadowOffset: {
      width: 0,
      height: 9
    },
    shadowOpacity: 0.5,
    shadowRadius: 12.35,
    elevation: 19
  },
  settingButton: {
    backgroundColor: "#285bde",
    shadowColor: "#808080",
    shadowOffset: {
      width: 0,
      height: 9
    },
    shadowOpacity: 0.5,
    shadowRadius: 12.35,
    elevation: 19
  },

  loginText: {
    marginLeft: 10,
    color: "white"
  }
});
