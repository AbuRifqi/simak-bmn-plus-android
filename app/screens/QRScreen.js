import * as React from "react";
import { Alert, Text, View, StyleSheet, Button } from "react-native";
import Constants from "expo-constants";
import * as Permissions from "expo-permissions";

import { BarCodeScanner } from "expo-barcode-scanner";
import BarcodeMask from "react-native-barcode-mask";
import { Container, Body, Content } from "native-base";
import Api from "../Api";
import Headers from "./Headers";

export default class QRCodeScanner extends React.Component {
  state = {
    hasCameraPermission: null,
    scanned: false,
    loading:false,
  };

  async componentDidMount() {
    this.getPermissionsAsync();
  }

  getPermissionsAsync = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === "granted" });
  };

  render() {
    const { hasCameraPermission, scanned } = this.state;

    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>;
    }
    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    }
    return (
      <Container>
        <Headers
          title = "Pindai kode QR"
          navigation={this.props.navigation}
          handlePostClick={this.handlePostClick}
        />
        <View
          style={{
            flex: 1,
            flexDirection: "column",
            justifyContent: "flex-end"
          }}
        >
          <BarCodeScanner
            onBarCodeScanned={scanned ? undefined : this.handleBarCodeScanned}
            style={StyleSheet.absoluteFillObject}
          />
          <BarcodeMask
            width={300}
            height={300}
            showAnimatedLine={true}
            transparency={0.8}
          />
          {scanned && (
            <Button
              title={"Tap to Scan Again"}
              onPress={() => this.setState({ scanned: false })}
            />
          )}
        </View>
      </Container>
    );
  }

  handleBarCodeScanned = ({ type, data }) => {
    try {
      this.setState({ scanned: true });
      const param = JSON.parse(data);
      if (
        param.kodeBarang == null ||
        param.nup == null ||
        param.kodeLokasi == null ||
        param.kodeBarang == "" ||
        param.nup == "" ||
        param.kodeLokasi == ""
      ) {
        Alert.alert(
          "Perhatian",
          "Kode QR tidak valid, data tidak bisa ditemukan!",
          [{ text: "OK", onPress: () => this.props.navigation.popToTop() }],
          { cancelable: false }
        );
        return;
      }
      this.fetchAsetBmn(param.kodeLokasi, param.kodeBarang,param.nup);
    } catch (error) {
      Alert.alert(
        "Proses Gagal",
        "Ma'af, proses pemindaian gagal, kemungkinan kode QR ini tidak valid!",
        [{ text: "OK", onPress: () => this.props.navigation.popToTop() }],
        { cancelable: false }
      );
    }
  };

  fetchAsetBmn = (kodeUakpb, kodeBarang, nup) => {
    this.setState({ loading: true });
    const params = this.props.navigation.state.params;

    let apiUrl =
      global.urlServer +
      "/api-android/get-aset/?kodeUakpb=" + kodeUakpb +
      "&kodeBarang=" +  kodeBarang+
      "&nup=" + nup;

      console.log ("ASET BMN = " + apiUrl);
    let options = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    };

    fetch(apiUrl, options)
      .then(response => response.json())
      .then(response => {
        this.setState({
          loading: false
        });
            console.log("HASIL ASET =" + JSON.stringify(response));
          if (response.data.nama_aset != null || response.data.nama_aset!="") {
            this.props.navigation.navigate("ImageList", {
              name: "from parent",
              rincianAset:response.data,
              isMonitoring:false,
            });
          } else {
            Alert.alert(
              "Informasi",
              "Ma'af, data yang dipindai tidak ditemukan di dalam database!",
              [{ text: "OK", onPress: () => this.props.navigation.popToTop() }],
              { cancelable: false }
            );
          }
      
      })
      .catch(function(error) {
        Alert.alert(
          "Terjadi Kesalahan",
          "Alamat server keliru atau tidak ada koneksi. \n" + error.message,
          [
            {
              text: "OK",
              onPress: () => {
                this.state.loading = false;
              }
            }
          ],
          { cancelable: false }
        );
      });
  };

  fetchImageList = items => {
    const params = this.props.navigation.state.params;

    
  };
}
