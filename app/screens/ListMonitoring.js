import React, { Component, useState } from "react";
import {
  StyleSheet,
  Alert,
  View,
  Button,
  ScrollView,
  StatusBar,
  ImageBackground,
  ActivityIndicator
} from "react-native";
import { FlatList } from "react-native-gesture-handler";
import { ListItem, SearchBar } from "react-native-elements";

import Moment from "moment";
import Constant from "../Constant";
import Api from "../Api";
// import Loader from "../Loader";
import Loader from "./Helper/Loader";
import { Fab, Icon, Text, Right } from "native-base";
import Headers from "./Headers";

export default class ListMonitoring extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      data: null,
      error: null,
      init: true,
      searchText: "",
      filteredData: [],
      isMonitoring:true,
    };
    arrayholder = [];
    rowNumber = 0;
  }

  renderHeader = () => {
    return (
      <SearchBar
        placeholder="Ketik apa yang ingin dicari..."
        lightTheme
        round
        onChangeText={this.search}
        autoCorrect={false}
        value={this.state.searchText}
      />
    );
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginLeft: "14%"
        }}
      />
    );
  };

  search = searchText => {
    rowNumber=0;
    if (this.state.data ==null ||this.state.data.length<=0 ) return;
    this.setState({ searchText: searchText });

    let filteredData = this.state.data.filter(function(item) {

      const itemData = `${
        item.nama_satker == null ? "" : item.nama_satker.toUpperCase()
      } ${
        item.tanggal_mulai == null ? "" : item.tanggal_mulai
      } ${
        item.tanggal_berakhir == null ? "" : item.tanggal_berakhir
      } ${
        item.lokasi == null ? "" : item.lokasi.toUpperCase()
      }`;
      const textData = searchText.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });

    this.setState({ filteredData: filteredData });
  };

  searchFilterFunction = text => {
    rowNumber=0;
    if (this.arrayholder && this.arrayholder.length > 0) {
      const newData = this.arrayholder.filter(item => {
        const itemData = `${
          item.nama_satker == null ? "" : item.nama_satker.toUpperCase()
        } ${
          item.tanggal_mulai == null ? "" : item.tanggal_mulai
        } ${
          item.tanggal_berakhir == null ? "" : item.tanggal_berakhir
        } ${
          item.lokasi == null ? "" : item.lokasi.toUpperCase()
        }`;
          const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      this.setState({
        data: newData
      });
    }
  };

  _keyExtractor = (item, index) => {
    return item.id;
  };

  fetchListMonitoring = () => {
    rowNumber=0;
    this.setState({ loading: true });
    const params = this.props.navigation.state.params;

    let apiUrl =
      global.urlServer +
      "/api-android/get-monitoring-aset/?kodeUakpb=" +
      global.userData.kodesatker;

    //   console.log ("ASET LIST = " + apiUrl);
    let options = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    };

    fetch(apiUrl, options)
      .then(response => response.json())
      .then(response => {
        this.setState({
          data: response.data,
          loading: false
        });
      })
      .catch(function(error) {
        Alert.alert(
          "Terjadi Kesalahan",
          "Alamat server keliru atau tidak ada koneksi. \n" + error.message,
          [
            {
              text: "OK",
              onPress: () => {
                this.state.loading = false;
              }
            }
          ],
          { cancelable: false }
        );
      });
  };

  renderHeaderItem = item => {
    rowNumber++;
    return (
      <View style={styleItems.containerHeader}>
        <View style={styleItems.containerNoAset}>
          <Text style={{
            fontSize:13,
            textAlign:"center",
            color:"white"
          }}>{rowNumber}</Text>
      </View>
        <View style={styleItems.containerKodeAset}>
          <View style={styleItems.containerBingkaiKode}>
            <Text style={[styles.setColorBlue]}>SatKer: {item.nama_satker}</Text>
          </View>
        </View>
      </View>
      
    );
  };

  renderStatus = item => {
    let kondisi= item.status_monitoring>0?"Sudah Dimonitoring":"Belum Dimonitoring";
    
    return (
      <Text style={{ fontSize: 14, fontWeight: "bold", color: "#c4402f" }}>
        Kondisi : {kondisi}
      </Text>
    );
  };

  subTitle(item) {
    return (
      <View
        style={{
          flex: 1,
          alignItems: "flex-start",
          height: 50,
          paddingHorizontal: 20
        }}
      >
        <Text style={{ color: "grey", fontSize: 11 }}>
          Tanggal Monitoring: {Moment(item.tanggal_mulai).format("DD MMM YYYY")} s/d {Moment(item.tanggal_berakhir).format("DD MMM YYYY")}
        </Text>
        <Text style={{ color: "grey", fontSize: 11 }}>
            Lokasi: {item.lokasi}
        </Text>
        {this.renderStatus(item)}
      </View>
    );
  }

  fetchMonitoringImage = (isMonitoring)=>{
    this.setState({isMonitoring:isMonitoring});
    if (this.state.isMonitoring)return null;
    this.setState({isMonitoring:true});
    this.fetchListMonitoring();
  }

  fetchListAset = items => {
    this.props.navigation.navigate("ListAsetMonitoring", {
      name: "from parent",
      rincianAset:items.item,
      isMonitoring:this.fetchMonitoringImage,
    });
  };

  async componentDidMount() {
    rowNumber=0;
    this.setState({ init: false });
    this.fetchListMonitoring();
  }

  render() {
    if (this.state.loading) {
      return <Loader />;
    }
    return (
      <View style={{flex:1}}>
        <Headers
          title={"Permintaan Monitoring"}
          navigation={this.props.navigation}
          handlePostClick={this.handlePostClick}
        />
        <StatusBar backgroundColor="#ff88e5" barStyle="light-content" />
        <FlatList
          data={
            this.state.filteredData && this.state.filteredData.length > 0
              ? this.state.filteredData
              : this.state.data
          }
          renderItem={({ item }) => (
            <ListItem
              key={item.id}
              roundAvatar
              title={this.renderHeaderItem(item)}
              subtitle={this.subTitle(item)}
              // avatar={{ uri: item.picture.thumbnail }}
              containerStyle={{ borderBottomWidth: 0 }}
              onPress={() => this.fetchListAset({ item })}
            />
          )}
          keyExtractor={this._keyExtractor}
          ItemSeparatorComponent={this.renderSeparator}
          ListHeaderComponent={this.renderHeader}
        />
        <View>
          <Fab
              style={{ backgroundColor: 'green' }}
              position="bottomRight"
              onPress={() => this.fetchListMonitoring()}>
              <Icon type="FontAwesome5" name="sync" />
          </Fab>
        </View>
      </View>
    );
  }
}

const styleItems=StyleSheet.create({
  container:{

  },
  containerHeader:{
    flexDirection:"row",
    flex :1
  },
  containerNoAset:{
    borderRadius:30, 
    width:60,height:60,
    borderColor:"black",
    backgroundColor:"blue", 
    borderWidth:2,
    justifyContent:"center",
    alignItems:"center",
    shadowColor: "#808080",
    shadowOffset: {
      width: 3,
      height: 9
    },
    shadowOpacity: 0.5,
    shadowRadius: 5.10,
    elevation: 10  
  },
  containerKodeAset:{
    flexDirection:"row",
    justifyContent:"center",
    alignItems:"center"
  },
  containerBingkaiKode:{
    borderColor:"blue",
    backgroundColor:"white",
    borderWidth:2,
    borderRadius:20,
    justifyContent:"center",
    alignItems:"center",
    padding:5,
    margin:5,
    height:40,
    shadowColor: "#808080",
shadowOffset: {
  width: 3,
  height: 9
},
shadowOpacity: 0.5,
shadowRadius: 5.10,
elevation: 10  },


});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F5FCFF"
  },
  indicator: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    height: 80
  },
  setColorRed: {
    color: "#f44336"
  },
  setColorPink: {
    color: "#e91e63"
  },
  setColorYellow: {
    color: "#FF9900"
  },
  setColorBlue: {
    color: "#0000FF"
  },
  lottie: {
    width: 100,
    height: 100
  }
});
