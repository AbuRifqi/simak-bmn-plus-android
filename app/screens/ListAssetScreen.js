import React, { Component, useState } from "react";
import {
  StyleSheet,
  Alert,
  View,
  Button,
  ScrollView,
  StatusBar,
  ImageBackground,
  ActivityIndicator
} from "react-native";
import { FlatList } from "react-native-gesture-handler";
import { ListItem, SearchBar } from "react-native-elements";

import Moment from "moment";
import Constant from "../Constant";
import Api from "../Api";
// import Loader from "../Loader";
import Loader from "./Helper/Loader";
import { Fab, Icon, Text, Right } from "native-base";
import Headers from "./Headers";

export default class ListAsetMonitoring extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      data: null,
      error: null,
      init: true,
      searchText: "",
      filteredData: []
    };
    arrayholder = [];
  }

  renderHeader = () => {
    return (
      <SearchBar
        placeholder="Ketik apa yang ingin dicari..."
        lightTheme
        round
        onChangeText={this.search}
        autoCorrect={false}
        value={this.state.searchText}
      />
    );
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginLeft: "14%"
        }}
      />
    );
  };

  search = searchText => {
    if (this.state.data ==null ||this.state.data.length<=0 ) return;
    this.setState({ searchText: searchText });

    let filteredData = this.state.data.filter(function(item) {

      const itemData = `${
        item.merk_type == null ? "" : item.merk_type.toUpperCase()
      } ${
        item.no_aset == null ? "" : item.no_aset
      }`;
      const textData = searchText.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });

    this.setState({ filteredData: filteredData });
  };

  searchFilterFunction = text => {
    if (this.arrayholder && this.arrayholder.length > 0) {
      const newData = this.arrayholder.filter(item => {
        const itemData = `${
          item.merk_type == null ? "" : item.merk_type.toUpperCase()
        } ${
          item.no_aset == null ? "" : item.no_aset
        }`;
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      this.setState({
        data: newData
      });
    }
  };

  _keyExtractor = (item, index) => {
    return item.no_aset;
  };

  fetchListAsetBmn = () => {
    this.setState({ loading: true });
    const params = this.props.navigation.state.params;

    let apiUrl =
      global.urlServer +
      "/api-android/get-aset-list/?kodeUakpb=" +
      params.kodeUakpb +
      "&kodeBarang=" +
      params.kodeBarang;

      console.log ("ASET LIST = " + apiUrl);
    let options = {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    };

    fetch(apiUrl, options)
      .then(response => response.json())
      .then(response => {
        this.setState({
          data: response.data,
          loading: false
        });
      })
      .catch(function(error) {
        Alert.alert(
          "Terjadi Kesalahan",
          "Alamat server keliru atau tidak ada koneksi. \n" + error.message,
          [
            {
              text: "OK",
              onPress: () => {
                this.state.loading = false;
              }
            }
          ],
          { cancelable: false }
        );
      });
  };

  renderHeaderItem = item => {
    return (
      <View style={styleItems.containerHeader}>
        <View style={styleItems.containerNoAset}>
          <Text style={{
            fontSize:10,
            textAlign:"center",
            color:"white"
          }}>No. Aset</Text>
          <Text style={{
            fontSize:15,
            fontWeight:"bold",
            textAlign:"center",
            color:"white"
          }}>{item.no_aset}</Text>
      </View>
        <View style={styleItems.containerKodeAset}>
          <View style={styleItems.containerBingkaiKode}>
            <Text style={[styles.setColorBlue]}>{item.uraian}</Text>
          </View>
        </View>
      </View>
      
    );
  };

  renderStatus = item => {
    let kondisi = this.renderKondisi(item.kondisi);
    return (
      <Text style={{ fontSize: 14, fontWeight: "bold", color: "#c4402f" }}>
        Kondisi : {kondisi}
      </Text>
    );
  };

  renderKondisi(param) {
    switch (parseInt(param)) {
      case 1:
        return "Baik";
      case 2:
        return "Rusak Ringan";
      case 3:
        return "Rusak Berat";
      default:
        return "Tidak Diketahui";
    }
  }

  subTitle(item) {
    return (
      <View
        style={{
          flex: 1,
          alignItems: "flex-start",
          height: 90,
          paddingHorizontal: 20
        }}
      >
        <Text>Merk/Type: {item.merk_type} </Text>
        <Text style={{color:"green",fontWeight:"bold"}}>Nilai: {item.nilaibmn} </Text>
        <Text style={{ color: "grey", fontSize: 11 }}>
          Tanggal Peroleh: {Moment(item.tgl_perlh).format("DD MMM YYYY")}
        </Text>
        <Text style={{ color: "grey", fontSize: 11 }}>
            Keterangan: {item.keterangan}
        </Text>
        {this.renderStatus(item)}
      </View>
    );
  }

  fetchImageList = items => {
    const params = this.props.navigation.state.params;

    this.props.navigation.navigate("ImageList", {
      name: "from parent",
      rincianAset:items.item,
      isMonitoring:false,
    });
  };

  async componentDidMount() {
    this.setState({ init: false });
    this.fetchListAsetBmn();
  }

  render() {
    if (this.state.loading) {
      return <Loader />;
    }
    return (
      <View style={{flex:1}}>
        <Headers
          title={"Daftar Aset"}
          navigation={this.props.navigation}
          handlePostClick={this.handlePostClick}
        />
        <StatusBar backgroundColor="#ff88e5" barStyle="light-content" />
        <FlatList
          data={
            this.state.filteredData && this.state.filteredData.length > 0
              ? this.state.filteredData
              : this.state.data
          }
          renderItem={({ item }) => (
            <ListItem
              key={item.no_aset}
              roundAvatar
              title={this.renderHeaderItem(item)}
              subtitle={this.subTitle(item)}
              // avatar={{ uri: item.picture.thumbnail }}
              containerStyle={{ borderBottomWidth: 0 }}
              onPress={() => this.fetchImageList({ item })}
            />
          )}
          keyExtractor={this._keyExtractor}
          ItemSeparatorComponent={this.renderSeparator}
          ListHeaderComponent={this.renderHeader}
        />
        <View>
          <Fab
              style={{ backgroundColor: 'green' }}
              position="bottomRight"
              onPress={() => this.fetchListAsetBmn()}>
              <Icon type="FontAwesome5" name="sync" />
          </Fab>
        </View>
      </View>
    );
  }
}

const styleItems=StyleSheet.create({
  container:{

  },
  containerHeader:{
    flexDirection:"row",
    flex :1
  },
  containerNoAset:{
    borderRadius:30, 
    width:60,height:60,
    borderColor:"black",
    backgroundColor:"blue", 
    borderWidth:2,
    justifyContent:"center",
    alignItems:"center",
    shadowColor: "#808080",
    shadowOffset: {
      width: 3,
      height: 9
    },
    shadowOpacity: 0.5,
    shadowRadius: 5.10,
    elevation: 10  
  },
  containerKodeAset:{
    flexDirection:"row",
    justifyContent:"center",
    alignItems:"center"
  },
  containerBingkaiKode:{
    borderColor:"blue",
    backgroundColor:"white",
    borderWidth:2,
    borderRadius:20,
    justifyContent:"center",
    alignItems:"center",
    padding:5,
    margin:5,
    height:40,
    shadowColor: "#808080",
shadowOffset: {
  width: 3,
  height: 9
},
shadowOpacity: 0.5,
shadowRadius: 5.10,
elevation: 10  },


});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F5FCFF"
  },
  indicator: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    height: 80
  },
  setColorRed: {
    color: "#f44336"
  },
  setColorPink: {
    color: "#e91e63"
  },
  setColorYellow: {
    color: "#FF9900"
  },
  setColorBlue: {
    color: "#0000FF"
  },
  lottie: {
    width: 100,
    height: 100
  }
});
