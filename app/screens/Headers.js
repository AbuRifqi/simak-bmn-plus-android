import React, { Component } from "react";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title,
  Text
} from "native-base";
import {
  TouchableOpacity,
  StyleSheet,
  StatusBar,
  Platform
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";

const Headers = ({ navigation, title,menu,handlePostClick }) => (
  <Header style={head.container} androidStatusBarColor="#1e88e5">
    <LinearGradient
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 0 }}
      locations={[0.2, 1]}
      style={{
        flex: 1,
        flexDirection: "row"
      }}
      colors={["#031b57", "#7a8fc2"]}
    >
      <Left>
        <Button transparent onPress={() => navigation.pop()}>
          <Icon name="arrow-back" />
        </Button>
      </Left>
      <Body >
        <Title>{title}</Title>
      </Body>
      <Right>
        {menu}
        {/* <TouchableOpacity onPress={() => handlePostClick()}>
        <Text style={{color: "#fff", top : 1, marginRight:10}}>Edit</Text>
      </TouchableOpacity> */}
      </Right>
    </LinearGradient>
  </Header>
);
const head = StyleSheet.create({
  container: {
    backgroundColor: "#031b57",
    ...Platform.select({
      android: {
        marginTop: StatusBar.currentHeight
      }
    })
  }
});
export default Headers;
