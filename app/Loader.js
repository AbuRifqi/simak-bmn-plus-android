import React from 'react';
import { ImageBackground,StyleSheet } from 'react-native';
import AnimatedLoader from "react-native-animated-loader";

export default class Loader extends React.Component {
    constructor(props) {
        super(props);
        this.state = { visible: true };
    }

    render() {
        const { visible } = this.state;
        return ( 
            <ImageBackground
            style={{ flex: 1 }}
            source={require("./../assets/backgrounds/home.jpg")}
          >
<
            AnimatedLoader visible = { visible }
            overlayColor = "rgba(255,255,255,0.5)"
            source = { require("../assets/animations/142-loading-animation.json") }
            animationStyle = { styles.lottie }
            speed = { 3.5 }
            />
          </ImageBackground>
            
        );
    }
}

const styles = StyleSheet.create({
    lottie: {
        width: 400,
        height: 400
    }
});