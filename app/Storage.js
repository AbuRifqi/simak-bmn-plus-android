import {AsyncStorage } from 'react-native'
const Storage = {
    async saveData(param,data) {
        await AsyncStorage.setItem(param, data)
        .then( ()=>{
            console.log('It was saved successfully')
        } )
        .catch( ()=>{
            console.log('There was an error saving the product')
        } )
    },

    async getData(param){
        try {
            const retrievedItem =  await AsyncStorage.getItem(param);
            const item = retrievedItem;
            return item;
        } catch (error) {
            console.log(error.message);
        }
        return null;
    },
    async getUserData(){
        return await this.getData('@user');
    },
    async saveUserData(data){
        return this.saveData('@user',data);
    },
    async getURLServer(){
        return await this.getData('@urlserver');
    },
    async saveURLServer(data){
        return this.saveData('@urlserver',data);
    }

}

export default Storage;
