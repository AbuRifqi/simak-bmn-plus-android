// import fetch from 'react-native-fetch-polyfill';
// import Constant from "./Constant";
// import moment from "moment";
// import axios from "axios";
import { Alert } from "react-native";

const timeoutInterval = 10 * 1000;
const Api = {
  async fetchLogin(username, password, onSuccess, onError) {
    let url =
      global.urlServer + "/api-android/login?&username=" +
      username +
      "&password=" +
      password;
    // let formData = new FormData();
    // formData.append("username",username);
    // formData.append("password",password);
    return new Promise(function(resolve, reject) {
      fetch(url, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type":"application/json" //"multipart/form-data"//
        },
        // body: formData,
      })
        .then(response => response.json())
        .then(response => {
          if (response.success) {
            resolve(onSuccess(response));
          } else {
            Alert.alert(
              "Proses Gagal",
              "Ma'af, proses login tidak berhasil",
              [{ text: "OK", onPress: () => console.log("OK Pressed") }],
              { cancelable: false }
            );
            resolve(onError(response.error));
          }
        })
        .catch(function(error) {
          Alert.alert(
            "Terjadi Kesalahan",
            "Alamat server keliru atau tidak ada koneksi. \n" + error.message,
            [{ text: "OK", onPress: () => console.log("OK") }],
            { cancelable: false }
          );
          resolve(onError(""));
          // reject(new Error(`Unable to retrieve events.\n${error.message}`));
        });
    });

    // return axios.post(
    //   url,
    //   {},{timeout: timeoutInterval*3})
    //   .then((response) => {
    //     onSuccess(response.data);
    //   })
    //   .catch(error => {
    //     console.log(error);
    //     onError(error);
    //   })
  },
  async fetchSimpanKondisiBarang(
    kodeBarang,
    nup,
    kodeUakpb,
    userId,
    newKondisi,
    keterangan,
    onSuccess,
    onError
  ) {
    let url =
      global.urlServer +
      "/api-android/simpan-kondisi-barang/?kodeBarang=" +
      kodeBarang +
      "&nup=" +
      nup +
      "&kodeUakpb=" +
      kodeUakpb +
      "&userId=" +
      userId +
      "&newKondisi=" +
      newKondisi +
      "&keterangan=" +
      keterangan;

      return fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        kodeBarang: kodeBarang,
        nup: nup,
        userId:userId,
        kodeUakpb: kodeUakpb,
        newKondisi: newKondisi
      })
    })
      .then(response => response.json())
      .then(response => {
        if (response.success) {
          onSuccess(response.data);
        } else {
          onError(response.error);
        }
      })
      .catch(error => {
        console.error(error);
        onError(error);
      })
      .done();

    // return axios.post(
    //   url,
    //   {},{timeout: timeoutInterval*3})
    //   .then((response) => {
    //     onSuccess(response.data);
    //   })
    //   .catch(error => {
    //     console.log(error);
    //     onError(error);
    //   })
  },
  async fetchUpdateMonitoring(
    id,
    onSuccess,
    onError
  ) {
    let url =
      global.urlServer +
      "/api-android/update-data-monitoring/?idMonitoring=" + id;

      return fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
    })
      .then(response => response.json())
      .then(response => {
        onSuccess(response.data);
      })
      .catch(error => {
        console.error(error);
        onError(error);
      })
      .done();
  },
  async fetchGetFotoBarang(kodeUakpb, kodeBarang, nup) {
    let url =
      global.urlServer +
      "/api-android/bmndefaultimage/?reg=" +
      kodeUakpb +
      "." +
      kodeBarang +
      "." +
      nup;

    return fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        kodeBarang: kodeBarang,
        nup: nup,
        kodeUakpb: kodeUakpb,
        newKondisi: newKondisi
      })
    })
      .then(response => response.json())
      .then(response => {
        if (response.success) {
          onSuccess(response.data);
        } else {
          onError(response.error);
        }
      })
      .catch(error => {
        console.error(error);
        onError(error);
      })
      .done();

    // return axios.post(
    //   url,
    //   {},{timeout: timeoutInterval*3})
    //   .then((response) => {
    //     onSuccess(response.data);
    //   })
    //   .catch(error => {
    //     console.log(error);
    //     onError(error);
    //   })
  },
  async fetchGetListKodeUakpb(onSuccess, onError) {
    let url =
      global.urlServer +
      "/api-android/get-listkodeuakpb";

    return fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
    })
      .then(response => response.json())
      .then(response => {
          onSuccess(response);
      })
      .catch(error => {
        console.error(error);
        onError(error);
      })
      .done();
  },
  async fetchGetListKodeBarang(onSuccess, onError) {
    let url =
      global.urlServer +
      "/api-android/get-listkodeuakpb";

    return fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
    })
      .then(response => response.json())
      .then(response => {
          onSuccess(response);
      })
      .catch(error => {
        console.error(error);
        onError(error);
      })
      .done();
  },
};

export default Api;
