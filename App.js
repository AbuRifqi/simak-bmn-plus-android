import React, { Component } from "react";
import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from 'react-navigation';

import HomeScreen from "./app/screens/HomeScreen"
import InputSearchScreen from "./app/screens/InputSearchScreen"
import SearchScreen from "./app/screens/SearchScreen"
// import CameraScreen from "./app/screens/CameraScreen"
import QRScreen from "./app/screens/QRScreen"
import ListAssetScreen from "./app/screens/ListAssetScreen"
import ListMonitoring from "./app/screens/ListMonitoring"
import ListAsetMonitoring from "./app/screens/ListAsetMonitoring"
import ImageListScreen from "./app/screens/ImageListScreen"
import ZoomImageScreen from "./app/screens/ZoomImageScreen"

import { AppLoading } from 'expo';


const Rootstack = createStackNavigator({
    Home: {
        screen: HomeScreen,
        navigationOptions: ({ navigation }) => ({
            header: null
        }),
    },
    Search: {
        screen: SearchScreen,
        navigationOptions: ({ navigation }) => ({
            header: null
        }),
    },
    InputSearch: {
        screen: InputSearchScreen,
        navigationOptions: ({ navigation }) => ({
            header: null
        }),
    },
    ListAsset: {
        screen: ListAssetScreen,
        navigationOptions: ({ navigation }) => ({
            header: null
        }),
    },
    ListMonitoring: {
        screen: ListMonitoring,
        navigationOptions: ({ navigation }) => ({
            header: null
        }),
    },
    ListAsetMonitoring: {
        screen: ListAsetMonitoring,
        navigationOptions: ({ navigation }) => ({
            header: null
        }),
    },QRScreen : {
        screen : QRScreen,
        navigationOptions: ({ navigation }) => ({
          header : null
        }),
    },
    // CameraScreen : {
    //     screen : CameraScreen,
    //     navigationOptions: ({ navigation }) => ({
    //       header : null
    //     }),
    // },
    ImageList : {
        screen : ImageListScreen,
        navigationOptions: ({ navigation }) => ({
          header : null
        }),
    },
    ZoomImage : {
        screen : ZoomImageScreen,
        navigationOptions: ({ navigation }) => ({
          header : null
        }),
    },
    // Profile : {
    //     screen : ProfileScreen,
    //     navigationOptions: ({ navigation }) => ({
    //       header : null
    //     }),
    // },
}, 
{
    initialRouteName: 'Home',
    navigationOptions: {
        headerStyle: {
            backgroundColor: '#00b5ec',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
            left: 107,
        },
    },
})

const AppContainer = createAppContainer(Rootstack);


export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            isReady: false
        }

    }
    async _cacheResourcesAsync() {
        await Expo.Font.loadAsync({
            Roboto: require("native-base/Fonts/Roboto.ttf"),
            Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
        });
    }

    render() {
        if (!this.state.isReady) {
            return ( <
                AppLoading startAsync = { this._cacheResourcesAsync }
                onFinish = {
                    () => this.setState({ isReady: true }) }
                onError = { console.warn }
                />
            );
        }
        return <AppContainer/> ;
    }
}